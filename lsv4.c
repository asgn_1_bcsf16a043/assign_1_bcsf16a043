#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include<time.h>
#include<pwd.h>
#include<grp.h>

extern int errno;

int count=0;

void do_ls(char*);
void long_listing(char*);
void uidtouname(int);
void gidtogname(int);
int main(int argc, char* argv[]){
   if (argc == 1)
      do_ls(".");
   else{
      int i=0;
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	      do_ls(argv[i]);
      }
   }
   return 0;
}


void do_ls(char * dir)
{
   struct dirent * entry;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   errno = 0;

	//taking the file names into an array.
	struct dirent * entry2;
   	DIR * dp2 = opendir(dir);
	char arr[][];
	int i=0;


	while((entry2 = readdir(dp2)) != NULL){
         if(entry2 == NULL && errno != 0){
                perror("readdir failed");
                exit(1);
         }else{
		 if(entry2->d_name[0]=='.')
			 continue;
               //storing entries names in an array 
		   struct stat info2;
		   int rv2 = lstat(entry2->d_name, &info2);
		  strcpy(*arr[i],entry2->d_name);
		  count++;
  
	 }
   }
	printf("value of count %d\n",count);
	for(int j=0;j<i;j++){
		printf("directory name:%s",arr[j]);
	}

	//doing the real work 
   while((entry = readdir(dp)) != NULL){
         if(entry == NULL && errno != 0){
                perror("readdir failed");
                exit(1);
         }else{
		 if(entry->d_name[0]=='.')
			 continue;
                
		   struct stat info;
		   int rv = lstat(entry->d_name, &info);
			//char str_mode=long_listing(entry->d_name);
		  // printf(" %s", str_mode);
		   
		   long_listing(entry->d_name);
		   printf(" %ld", info.st_nlink);
		   uidtouname(info.st_uid);
		 //  printf(" %d", info.st_uid);
			gidtogname(info.st_gid);
		   //printf(" %d", info.st_gid);
		   printf(" %ld", info.st_size);
		   long secs=info.st_mtime;
		   printf(" %s",ctime(&secs));
		   printf(" %s\n",entry->d_name);
  
	 }
   }
   closedir(dp);
}

void long_listing(char *fp){
		
   struct stat buf;
   if (lstat(fp, &buf)<0){
      perror("Error in stat");
      exit(1);
   }
   int mode = buf.st_mode; 
   char str[10];
   strcpy(str, "---------");
//owner  permissions
   if((mode & 0000400) == 0000400) str[0] = 'r';
   if((mode & 0000200) == 0000200) str[1] = 'w';
   if((mode & 0000100) == 0000100) str[2] = 'x';
//group permissions
   if((mode & 0000040) == 0000040) str[3] = 'r';
   if((mode & 0000020) == 0000020) str[4] = 'w';
   if((mode & 0000010) == 0000010) str[5] = 'x';
//others  permissions
   if((mode & 0000004) == 0000004) str[6] = 'r';
   if((mode & 0000002) == 0000002) str[7] = 'w';
   if((mode & 0000001) == 0000001) str[8] = 'x';
//special  permissions
   if((mode & 0004000) == 0004000) str[2] = 's';
   if((mode & 0002000) == 0002000) str[5] = 's';
   if((mode & 0001000) == 0001000) str[8] = 't';
   printf("%s",str);
}

void uidtouname(int id){
int uid = id;
   errno = 0;
   struct passwd * pwd = getpwuid(uid);
  
   if (pwd == NULL){
      if (errno == 0)
         printf("Record not found in passwd file.\n");
      else
         perror("getpwuid failed");
   }
   else
       printf(" %s",pwd->pw_name);   

}

void gidtogname(int id){
int gid = id;
   struct group * grp = getgrgid(gid);
  
   errno = 0;
   if (grp == NULL){
      if (errno == 0)
          printf("Record not found in /etc/group file.\n");
      else
          perror("getgrgid failed");
   }else
      printf(" %s", grp->gr_name); 
}

